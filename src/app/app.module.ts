import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { MovieComponent } from './movie/movie.component';
import { MovieAddComponent } from './movie/movie-add/movie-add.component';
import { MovieSummaryComponent } from './movie/movie-summary/movie-summary.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    MovieAddComponent,
    MovieSummaryComponent,
    HoverHighlightDirective,
    HomeComponent,
    HeaderComponent,
    EnquiryComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
