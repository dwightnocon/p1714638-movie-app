import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css']
})
export class EnquiryComponent implements OnInit {

  emailBlackList = ['test@test.com', 'temp@temp.com'];

  enquiryForm: FormGroup;
  submitted = true;

  constructor() { }

  ngOnInit() {
    this.enquiryForm = new FormGroup({
      'username'   : new FormControl(null, [Validators.required, this.blankSpaces, Validators.minLength(5)]),
      'useremail'  : new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      'usermessage': new FormControl(null, [Validators.required, Validators.pattern("(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}")])
    });
  }

  
  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return {'emailBlackListed': true};
    }
    return null;
  }

  onSubmit() {
    console.log(this.enquiryForm);
    // this.submitted = true;
    console.log("Submitting Form");
  }

}
