export class Movie {
    public title : string;
    public genre : string;
    
    constructor (title: string, genre: string) {
        this.title = title;
        this.genre = genre;
    }
}