import { EventEmitter, Injectable } from '@angular/core';
import { Movie } from './movie.model'

@Injectable()
export class MoviesService {
  movieAdded = new EventEmitter<void>();

  private movieList : Movie [] = [
  new Movie('MIB', 'Sci-Fi')
];

  constructor() { }

  addMovie(newMovieInfo) {
    this.movieList.push(newMovieInfo);
    this.movieAdded.emit();
  }

  getMovies() {
    return this.movieList.slice();
  }
}
