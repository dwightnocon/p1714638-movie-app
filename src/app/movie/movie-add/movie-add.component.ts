import { Component, OnInit} from '@angular/core';
import { Movie } from "../movie.model";
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  inputInfo : Movie = new Movie ("", "");

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
  }

  onAddMovie(inputTitle: HTMLInputElement, inputGenre: HTMLInputElement) {

    this.moviesService.addMovie(new Movie(
      inputTitle.value,
      inputGenre.value
    ));
  }
}
