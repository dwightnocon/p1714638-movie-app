import { Component, OnInit } from '@angular/core';

import { Movie } from "./movie.model"
import { MoviesService } from './movies.service'

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  providers: [MoviesService]
})
export class MovieComponent implements OnInit {

  movieList : Movie [] = [];

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    this.movieList = this.moviesService.getMovies();
    this.moviesService.movieAdded
    .subscribe(() => {
      this.movieList = this.moviesService.getMovies();
    });
   }

  onMovieAdded (newMovieInfo) {
    console.log(newMovieInfo);
    this.movieList.push(newMovieInfo);
    console.log(this.movieList);
  }

}
