import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component'
import { MovieComponent } from './movie/movie.component'
import { EnquiryComponent } from './enquiry/enquiry.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'

const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent },
  {path: 'movie', component: MovieComponent },
  {path: 'enquiry', component: EnquiryComponent },
  {path: 'not-found', component: PageNotFoundComponent },
  {path: '**', redirectTo: '/not-found' }
]

@NgModule({
  declarations: [],

  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
